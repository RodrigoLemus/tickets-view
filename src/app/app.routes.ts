import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { TicketsComponent } from './tickets/tickets.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { SprintComponent } from './sprint/sprint.component';

const APP_ROUTES: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'tickets', component: TicketsComponent },
  { path: 'usuario', component: UsuarioComponent },
  { path: 'sprint', component: SprintComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'login' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash:true });